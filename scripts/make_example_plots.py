from pathlib import Path

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

from nx_pretty_plot.utils import draw_networkx_pretty, node2vec_layout

if __name__ == "__main__":
    graph_labels = [
        "Karate Club",
        "Davis Southern Women",
        "Florentine Families",
        "Les Miserables",
        "Ring of Cliques",
        "Random Tree",
        "Power Law Cluster",
        "Stochastic Block Model",
    ]

    # Create arguments for the stochastic block model
    sbm_mat = np.random.random(size=(15, 15)) * (3 * 10**-4) + (5 * 10**-4)
    np.fill_diagonal(sbm_mat, 0.9)
    sbm_mat = np.minimum(sbm_mat, sbm_mat.T)

    graphs = [
        nx.karate_club_graph(),
        nx.davis_southern_women_graph(),
        nx.florentine_families_graph(),
        nx.les_miserables_graph(),
        nx.ring_of_cliques(10, 10),
        nx.random_labeled_tree(200),
        nx.powerlaw_cluster_graph(500, 1, 0.001),
        nx.stochastic_block_model(np.random.randint(10, 20, 15), sbm_mat),
    ]

    Path("../figures").mkdir(exist_ok=True, parents=True)

    for label, graph in zip(graph_labels, graphs):
        pos = nx.drawing.spring_layout(graph)
        f_stem = label.strip().lower().replace(" ", "_")
        fig = draw_networkx_pretty(graph, pos=pos, title=label)
        plt.savefig(f"../figures/{f_stem}_spring.png")
        plt.close(fig)

        pos = node2vec_layout(graph)
        fig = draw_networkx_pretty(graph, pos=pos, title=label)
        plt.savefig(f"../figures/{f_stem}_node2vec.png")
        plt.close(fig)

        pos = node2vec_layout(graph, with_spring=True)
        fig = draw_networkx_pretty(graph, pos=pos, title=label)
        plt.savefig(f"../figures/{f_stem}_node2vec_spring.png")
        plt.close(fig)
