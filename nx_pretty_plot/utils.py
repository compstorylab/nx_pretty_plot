import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from matplotlib.collections import LineCollection
from node2vec import Node2Vec
from sklearn.decomposition import PCA

from .curved_edges import curved_edges


def draw_networkx_pretty(
    G,
    pos=None,
    title="",
    node_size=300,
    node_alpha=1.0,
    edge_alpha=0.75,
    with_labels=False,
):
    pos = pos or node2vec_layout(G)

    edges = curved_edges(G, pos)
    lc = LineCollection(edges, color="#7d7d7d", alpha=edge_alpha)

    fig, ax = plt.subplots()
    plt.axis("off")
    plt.title(title)

    ax.add_collection(lc)
    nx.draw_networkx_nodes(
        G,
        pos=pos,
        alpha=node_alpha,
        node_size=node_size,
        ax=ax,
        edgecolors="#0a5078",
    )

    if with_labels:
        nx.draw_networkx_labels(G, pos=pos)

    return fig


def node2vec_layout(G, with_spring=False):
    node2vec = Node2Vec(G, dimensions=300, workers=8)
    model = node2vec.fit()
    vectors = model.wv

    np_vecs = np.array([vectors[str(node)] for node in G.nodes])
    np_vecs = PCA(n_components=2).fit_transform(np_vecs)

    node2pos = {node: vector for node, vector in zip(G.nodes, np_vecs)}
    if with_spring:
        node2pos = nx.drawing.spring_layout(G, pos=node2pos, iterations=1)

    return node2pos
