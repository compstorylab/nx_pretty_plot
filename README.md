# `nx_pretty_plot`
Utilities to quickly create visualizations of networkx graphs that are visually pleasing and human readable.

Try running `make_example_plots.py` to see some pretty pictures.
`make_example_plots.py` also provides some example usages of the functions provided by `nx_pretty_plot`.
Take a look in `utils.py` if you want to see the implementation and additional options.